package net.tncy.lbe.myproject1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    @Test
    public void dividePositiveNumbers() {
        // Given
        Calculator calculator = new Calculator();
        // When
        int result = calculator.divide(12, 3);
        // Then
        assertEquals(4, result);
    }

    @Test(expected = ArithmeticException.class)
    public void divideByZero() {
        // Given
        Calculator calculator = new Calculator();
        // When
        calculator.divide(12, 0); // Exception expected
    }

}
